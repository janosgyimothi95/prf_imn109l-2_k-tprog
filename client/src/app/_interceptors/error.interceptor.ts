import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthenticationService } from '@app/_services';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private authenticationService: AuthenticationService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      if([401, 403].indexOf(err.status) !== -1){

        //logout if any of those errors appears
        this.authenticationService.logout();
        location.reload(true);
      }
      console.log(err)
      
      const error = err.error.message || err.error.statusText;
      return throwError(error);
    }))
  }
}
