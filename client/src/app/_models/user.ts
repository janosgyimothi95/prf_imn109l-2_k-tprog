import { Role } from './role';

export class User {
  username: string;
  nickname: string;
  role: Role;
  token?: string;
}
