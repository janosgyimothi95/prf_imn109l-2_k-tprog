export class Message {
    body: string;
    date: string;
    username: string;
}