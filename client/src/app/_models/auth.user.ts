import { Role } from './role';

export class AuthUser {
  id: number;
  username: string;
  password: string;
  role: Role;
  token?: string;
  nickname: string;
}
