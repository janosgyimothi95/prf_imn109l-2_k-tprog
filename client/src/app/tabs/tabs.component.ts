import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { SocketService } from '@app/_services/socket.service';
import { HttpClient } from '@angular/common/http';
import { DataService } from '@app/_services/data.service';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent implements OnInit {
  selectedTab: string = 'chatWithUsers';

  newMessage: any;
  messageList: string[] = [];

  onlineUsers: [] = [];
  topPlayers: any = [];

  constructor(
    private socketService: SocketService,
    private http: HttpClient,
    private changeDetector: ChangeDetectorRef,
    private dataService: DataService
  ) {}

  sendMessage() {
    this.socketService.sendMessage(this.newMessage);
    this.newMessage = '';
  }
  ngOnInit() {
    this.socketService.joinChatRoom();
    this.socketService.getMessages().subscribe((message: any) => {
      this.messageList.push(message);

      let scroll = document.getElementById('messageList');

      // Scroll to the last message
      setTimeout(() => {
        let messageObject = document.getElementById(`message-${message.username}-${message.date}`);
        scroll.scrollTop = scroll.scrollHeight + messageObject.offsetHeight;
      }, 100);
    });

    this.socketService.getOnlineUsers().subscribe((users) => {
      this.onlineUsers = users;
    });
  }

  ngOnUpdate() {
    this.socketService.joinChatRoom();
  }

  async onTabSelect(id: string) {
    this.selectedTab = id;

    if(this.selectedTab === 'topPlayers'){
      this.topPlayers = await this.dataService.getTopPlayers();
    }
  }
}
