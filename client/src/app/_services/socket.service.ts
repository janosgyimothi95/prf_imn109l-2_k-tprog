import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Message } from '../_models/message';
import { Observable } from 'rxjs';
import { WS_MESSAGES } from '@app/_constants/constants';
import { AuthenticationService } from './authentication.service';

// import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root',
})
export class SocketService {
  constructor(private socket: Socket, private authService: AuthenticationService) {}

  public joinChatRoom() {
    this.socket.emit(WS_MESSAGES.JOIN_CHAT, this.authService.currentUserValue);
  }

  public joinCompetition() {
    this.socket.emit(WS_MESSAGES.COMPETITION_JOIN);
  }

  public leaveCompetition() {
    this.socket.emit(WS_MESSAGES.COMPETITION_LEAVE);
  }

  public submitSolution(solution: any) {
    this.socket.emit(WS_MESSAGES.COMPETITION_SUBMIT_SOLUTION, {
      solution: solution,
      username: this.authService.currentUserValue?.nickname || this.authService.currentUserValue?.username,
    });
  }

  public sendMessage(msg: string) {
    let timeStamp = new Date().toLocaleTimeString();
    timeStamp = `${timeStamp.split(':')[0]}:${timeStamp.split(':')[1]}`;
    const message: Message = {
      body: msg,
      date: timeStamp,
      username: this.authService.currentUserValue.username,
    };
    this.socket.emit(WS_MESSAGES.NEW_MESSAGE, message);
  }

  public getMessages = () => {
    return Observable.create((observer) => {
      this.socket.on(WS_MESSAGES.NEW_MESSAGE, (message) => {
        observer.next(message);
      });
    });
  };

  public getNewTasks = () => {
    return Observable.create((observer) => {
      this.socket.on(WS_MESSAGES.COMPETITION_NEW_TASK, (task) => {
        observer.next(task);
      });
    });
  };

  public getWinningNotification = () => {
    return Observable.create((observer) => {
      this.socket.on(WS_MESSAGES.COMPETITION_WON, (score) => {
        observer.next(score);
      });
    });
  };

  public getPrepareNotification = () => {
    return Observable.create((observer) => {
      this.socket.on(WS_MESSAGES.COMPETITION_PREPARE, (task) => {
        observer.next(true);
      });
    });
  };

  public getOnlineUsers = () => {
    return Observable.create((observer) => {
      this.socket.on(WS_MESSAGES.ONLINE_PLAYERS, (onlineUsers) => {
        observer.next(onlineUsers);
      });
    });
  };
}
