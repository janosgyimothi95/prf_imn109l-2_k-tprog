import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { User, Role, AuthUser } from '@app/_models';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  public currentUser: Observable<User>;
  private currentUserSubject: BehaviorSubject<User>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string) {
    return this.http
      .post<any>(`${environment.apiUrl}/login`, { username, password })
      .pipe(
        map((data) => {
          if (data) {
            localStorage.setItem(
              'currentUser',
              JSON.stringify({ token: data.accessToken, username: username, role: data.role })
            );
            this.currentUserSubject.next({ username: username, role: data.role, nickname: data.nickname, token: data.accessToken });
          }

          return { username: username, token: data.token, role: data.role, nickname: data.nickname };
        })
      );
  }

  register(username: string, password: string, nickname: string, email?: string) {
    const credentials = { username, password, email, nickname };

    return this.http.post<any>(`${environment.apiUrl}/register`, credentials).pipe(
      map((data) => {
        if (data) {
          localStorage.setItem(
            'currentUser',
            JSON.stringify({ token: data.accessToken, username: username, role: data.role, nickname: data.nickname })
          );
          this.currentUserSubject.next({ username: username, role: data.role, nickname: data.nickname });
        }
        return { username: username, token: data.token, role: data.role };
      })
    );
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
