import { Component, OnInit } from '@angular/core';
import { SocketService } from '@app/_services/socket.service';

@Component({
  selector: 'app-competition',
  templateUrl: './competition.component.html',
  styleUrls: ['./competition.component.scss'],
})
export class CompetitionComponent implements OnInit {
  solution: string;
  currentTask: string;
  won: boolean;
  prepare: boolean = true;
  headerText: string;
  points: string = '12';
  earnedPoints: string;
  timeLeft: number;

  timeDecrementTimer;

  constructor(private socketService: SocketService) {}

  ngOnInit(): void {
    this.socketService.joinCompetition();
    this.socketService.getNewTasks().subscribe((task) => {
      this.prepare = false;

      clearInterval(this.timeDecrementTimer);

      this.currentTask = task.task;
      this.points = task.score;
      this.timeLeft = task.time;

      this.timeDecrementTimer = setInterval(() => {
        if (this.timeLeft > 0) {
          this.timeLeft--;
        }
      }, 1000);
    });

    this.socketService.getWinningNotification().subscribe((score) => {
      this.won = true;
      this.earnedPoints = score;
    });

    this.socketService.getPrepareNotification().subscribe((prepare) => {
      clearInterval(this.timeDecrementTimer);
      this.solution = '';
      this.points = null;

      this.timeLeft = 0;

      this.currentTask = '';
      this.prepare = prepare;

        this.won = false;

    });

  }

  ngOnDestroy() {
    this.socketService.leaveCompetition();
  }

  submitSolution() {
    this.socketService.submitSolution(this.solution);
  }
}
