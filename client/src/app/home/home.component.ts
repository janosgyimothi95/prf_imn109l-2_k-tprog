import { Component } from '@angular/core';
import { SocketService } from '@app/_services/socket.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [SocketService],
})
export class HomeComponent {
  constructor() {}
}
