import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AuthenticationService } from '@app/_services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  inCompetition: boolean = false;

  username: string;
  constructor(private authService: AuthenticationService, private router: Router, private changeDetector: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.username = this.authService.currentUserValue.username;
  }

  ngAfterViewInit() {
    if(this.router.url.includes('/competition')) {
      this.inCompetition = true;
    }else{
      this.inCompetition = false;
    }

    // An elegant fix for conditional rendering.
    // We change the inCompetition flag based on url, which will change after the ngIf already checked the value of it.
    this.changeDetector.detectChanges();
  }
}
