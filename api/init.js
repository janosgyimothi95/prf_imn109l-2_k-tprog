const roles = require('./roles.js');

// Init script for default admin user. It will create an admin user

const admin = {
  username: 'admin',
  password: 'admin',
  email: 'admin@admin.com',
  role: roles.admin
};

const User = require('./models/user');

const initAdminUser = async () => {
  const db_user = await User.find({ username: admin.username });

  if (db_user.length === 0) {
    User.create(admin, (error, usr) => console.log('[LOG] Admin user successfully initialized.'));
  } else {
    console.log('[LOG] Admin user already initialized. Skipping..');
  }
};

module.exports = initAdminUser;
