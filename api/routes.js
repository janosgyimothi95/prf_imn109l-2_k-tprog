const router = require('express').Router();
const jwt = require('jsonwebtoken');

ACCESS_TOKEN_SECRET = 'asdléasdkrjojjavoáijfabésvkdf';
const roles = require('./roles.js');
const User = require('./models/user');

router.route('/login').post((req, res) => {
  const { username, password } = req.body;

  if (username && password) {
    // Custom method. Checks if an user exists with the given username, and compares the encrypted password to the give one.
    User.authenticate(username, password, (error, usr) => {
      if (error || !usr) {
        // No user found
        return res.status(400).send({ statusText: 'Hibás felhasználónév vagy a jelszó' });
      } else {
        // Successfull login
        const accessToken = jwt.sign({ username: usr.username, role: usr.role }, ACCESS_TOKEN_SECRET);
        res.status(200).json({ accessToken: accessToken, role: usr.role });
        console.log(`[LOGIN] ${username} logged in. `)
      }
    });
  } else {
    // No username and/or password given
    return res.status(400).send({ statusText: 'Hiányzik a felhasználónév vagy a jelszó' });
  }
});

router.route('/register').post(async (req, res) => {
  const { username, password, nick, email } = req.body;

  if (username && password) {
    const user = { username: username, password: password, role: roles.user, email: email };

    // Check if user exists in the db
    const db_user = await User.find({ username: username });

    // If not, then create one, and sign the token
    if (db_user.length === 0) {
      User.create(user, (error, usr) => {
        if (error) {
          console.log(error);
          return res.status(400).send({ statusText: 'Sikertelen regisztráció' });
        } else {
          // We don't want to send back the password in the token
          delete user.password;

          // Signing token
          const accessToken = jwt.sign(user, ACCESS_TOKEN_SECRET);

          // Sending token back
          console.log(`[REGISTER] ${username} registered. `)

          return res.json({ accessToken: accessToken }).status(200);
        }
      });
    } else {
      return res.status(400).send({ statusText: 'Már van ilyen felhasználónév!' });
    }
  } else {
    return res.status(400).send({ statusText: 'Hiányzik a felhasználónév vagy a jelszó' });
  }
});

router.route('/logout').post((req, res) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  if (token) {
    jwt.verify(token, ACCESS_TOKEN_SECRET, (err, user) => {
      if (err) return res.status(403).send({ statusText: 'Invalid JWT token' });
      res.status(200).send('Kijelentkezés sikeresen megtörtént');
    });
  }
  res.status(405).send('Missing JWT token');
});

router.route('/toplist').get(authenticateToken, async (req, res) => {
  console.log('Acces to toplist granted', req.user);

  const users = await User.find();
  const result = users.map((user) => {
    return { username: user.username, score: user.score };
  });
  result.sort((a, b) => {
    if (a.score < b.score) {
      return 1;
    } else if (a.score > b.score) {
      return -1;
    } else {
      return 0;
    }
  });
  res.status(200).json(result);
});

// Midlleware to check the token
function authenticateToken(req, res, next) {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  if (token == null) return res.sendStatus(401);

  jwt.verify(token, ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) return res.sendStatus(403);
    req.user = user;
    next();
  });
}

module.exports = router;
