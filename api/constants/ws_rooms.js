const WS_ROOMS = {
    CHAT: 'WS::ROOM::CHAT',
    COMPETITION: 'WS::ROOM::COMPETITION'
}

module.exports = WS_ROOMS;