const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const init = require('./init');
const rooms = require('./constants/ws_rooms');
const wsMessages = require('./constants/ws_messages');

const User = require('./models/user');

/// Task generator constants
const OPERATOR_LIST = [' + ', ' - ', ' * '];
const MIN_NUM_OF_OPERANDS = 2;
const MAX_NUM_OF_OPERANDS = 4;
const UPPER_NUM_BOUND = 30;
const LOWER_NUM_BOUND = 2;
const MIN_SCORE = 10;
const MAX_SCORE = 300;
const MIN_TIME = 15;
const MAX_TIME = 60;

var app = express();

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());

var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

let currentTask = {};
let competitionInProgress = false;

let connectedUsers = new Set();
let onlineUsers = [];

let validSolutions = [];

let last12Message = [];

// Competition timer
let timer;
let prepareTimer;
io.on('connection', (socket) => {
  console.log('user connected');

  socket.on('disconnect', () => {
    // Remove socketId from connected users
    connectedUsers.delete(socket.id);

    // Online users in chat // We have to delete the user from the online chat users
    const disconnectedUser = onlineUsers.find((user) => user.socketId === socket.id);

    // If the user was in chatroom, remove him
    onlineUsers = onlineUsers.filter((user) => user !== disconnectedUser);
    io.to(rooms.CHAT).emit(wsMessages.ONLINE_PLAYERS, onlineUsers);

    // Competition over
    if (connectedUsers.size < 2) {
      console.log('Not enough participants for a competition.');
      endCompetition();
      io.to(socket.id).emit(wsMessages.COMPETITION_PREPARE); ////////////////
    }

    console.log('user disconnected');
  });

  // Event listener -> Runs when user joins the chat.
  socket.on(wsMessages.JOIN_CHAT, (usr) => {
    console.log(`${usr.username} joined to room chat`);
    socket.join(rooms.CHAT);

    const user = { ...usr, socketId: socket.id, date: new Date().toLocaleTimeString() };

    if (!onlineUsers.find((user) => user.socketId === socket.id)) {
      onlineUsers.push(user);
    }

    io.to(rooms.CHAT).emit(wsMessages.ONLINE_PLAYERS, onlineUsers);

    last12Message.forEach((msg) => io.to(socket.id).emit(wsMessages.NEW_MESSAGE, msg));
  });

  // Event listener -> Runs when user joins the competition.
  socket.on(wsMessages.COMPETITION_JOIN, () => {
    console.log('Joined to competition room');
    // We keep track of connected users
    connectedUsers.add(socket.id);
    // Connect user to the competition room
    socket.join(rooms.COMPETITION);

    io.to(socket.id).emit(wsMessages.COMPETITION_PREPARE); ////////////////
    // If a competition is in progress, send the current task to the new user.
    if (!competitionInProgress && connectedUsers.size > 1) {
      // If there isn't a running competition and we have enough players, we start one.
      setTimeout(() => {
        startCompetition();
      }, 2000);
    }
  });
  // Event listener -> Runs when a user leaves the competition
  socket.on(wsMessages.COMPETITION_LEAVE, () => {
    console.log('A user left the competition room');
    connectedUsers.delete(socket.id);

    // Competition over
    if (connectedUsers.size < 2) {
      console.log('Not enough participants for a competition.');
      endCompetition();
      io.to(socket.id).emit(wsMessages.COMPETITION_PREPARE); ////////////////
    }

    socket.leave(rooms.COMPETITION);
  });

  // Event listener -> Runs when a new message is recieved, and then it's emitted to the chatroom.
  socket.on(wsMessages.NEW_MESSAGE, (msg) => {
    io.to(rooms.CHAT).emit(wsMessages.NEW_MESSAGE, msg);
    console.log(`[${msg.date}] ${msg.username}: ${msg.body}`);
    storeMessage(msg);
  });

  /////////////////////////////// Competition logic //////////////////////////////////

  // Event listener -> Runs when someone submits a solution to the task
  socket.on(wsMessages.COMPETITION_SUBMIT_SOLUTION, (data) => {
    const { username, solution } = data;
    // We will accept the first 3 or 2 (if we have 2 players) valid solutions.
    const requiredSolutionNumber = Math.min(connectedUsers.size, 3);

    // If the competition is running and we don't have enough solutions already, we will process the submission.
    if (validSolutions.length <= requiredSolutionNumber && competitionInProgress) {
      // Score value of the submission. Depends on the position of recieved submissions.
      const scoreValue = currentTask.score - validSolutions.length;

      // If it's a valid solution, and it wasn't accepted already we, will accept it.
      if (parseInt(solution) === currentTask.solution && !validSolutions.includes(username)) {
        // Updating the DB
        User.updateOne({ username: username }, { $inc: { score: scoreValue } }).then((res) => {
          console.log(`[SUBMISSION] ${username} earned ${scoreValue} points.`);
        });

        // For further existance validations.
        validSolutions.push(username);

        // Notify the user
        io.to(socket.id).emit(wsMessages.COMPETITION_WON, scoreValue);

        // Check if we reached the sumission treshold
        if (validSolutions.length === requiredSolutionNumber) {
          console.log(`Reached ${requiredSolutionNumber} submissions, starting new competition`);

          // End current competition and start a new one.
          endCompetition();

          // Notify the users to prepare for the new task.
          io.to(rooms.COMPETITION).emit(wsMessages.COMPETITION_PREPARE);

          // Wait 2s and start a new one.
          setTimeout(() => {
            startCompetition();
          }, 2000);
          validSolutions = [];
        }
      }
    }
  });
});

const getNextOperatorIndex = (operator_array) => {
  var arrayLength = operator_array.length;
  for (var i = 0; i < arrayLength; i++) {
    if (operator_array[i] === '*') {
      return i;
    }
  }
  return 0;
};

const doNextOperation = (equation_element_array) => {
  var operators_arr = [];
  var numbers_arr = [];

  for (var i = 0; i < equation_element_array.length; i++) {
    if (i % 2 === 0) {
      numbers_arr.push(equation_element_array[i]);
    } else {
      operators_arr.push(equation_element_array[i]);
    }
  }

  var selected_op_ind = getNextOperatorIndex(operators_arr);
  var selected_operator = operators_arr[selected_op_ind];

  var result = null;
  if (selected_operator == '*') {
    result = Number(numbers_arr[selected_op_ind]) * Number(numbers_arr[selected_op_ind + 1]);
  } else if (selected_operator == '+') {
    result = Number(numbers_arr[selected_op_ind]) + Number(numbers_arr[selected_op_ind + 1]);
  } else {
    result = Number(numbers_arr[selected_op_ind]) - Number(numbers_arr[selected_op_ind + 1]);
  }

  operators_arr.splice(selected_op_ind, 1);
  numbers_arr.splice(selected_op_ind, 1);
  numbers_arr.splice(selected_op_ind, 1, result);

  var new_eq = '';
  for (var i = 0; i < operators_arr.length; i++) {
    new_eq = new_eq + String(numbers_arr[i]) + ' ' + String(operators_arr[i]) + ' ';
  }
  new_eq += String(numbers_arr[numbers_arr.length - 1]);

  return new_eq;
};

const solveEquation = (string_equation) => {
  var element_arr = string_equation.split(' ');
  var new_eq = null;

  while (element_arr.length > 1) {
    new_eq = doNextOperation(element_arr);
    element_arr = new_eq.split(' ');
  }
  return element_arr[0];
};

const calculateScore = (number_arr, operator_arr) => {
  var num_of_ops = operator_arr.length * 0.75;
  var op_diff = 1;
  if (operator_arr.includes(' * ')) {
    op_diff = 5;
  }
  var sum_num_size = 0;
  for (var i = 0; i < number_arr.length; i++) {
    sum_num_size += Number(number_arr[i]);
  }
  var avg_num_size = sum_num_size / number_arr.length;
  var score = Math.floor(num_of_ops * op_diff * avg_num_size);
  if (score > MAX_SCORE) {
    score = MAX_SCORE;
  } else if (score < MIN_SCORE) {
    score = MIN_SCORE;
  }
  return score;
};

const calculateTime = (score) => {
  var time = Math.floor(score / 6.5);
  if (time > MAX_TIME) {
    time = MAX_TIME;
  } else if (time < MIN_TIME) {
    time = MIN_TIME;
  }
  return time;
};

const getNewTask = () => {
  var task,
    solution,
    score,
    time,
    multip_flag = false;
  var num_of_numbers = Math.floor(Math.random() * MAX_NUM_OF_OPERANDS) + MIN_NUM_OF_OPERANDS;

  var numbers = [];
  var operators = [];
  for (var i = 0; i < num_of_numbers; i++) {
    if (i > 0) {
      multip_flag = operators.includes(' * ');
      if (multip_flag) {
        operators.push(OPERATOR_LIST[Math.floor(Math.random() * 2)]);
      } else {
        operators.push(OPERATOR_LIST[Math.floor(Math.random() * 3)]);
      }
    }
    numbers.push(Math.floor(Math.random() * UPPER_NUM_BOUND) + LOWER_NUM_BOUND);
  }

  task = String(numbers[0]);
  for (var i = 0; i < num_of_numbers - 1; i++) {
    task += operators[i] + String(numbers[i + 1]);
  }

  solution = solveEquation(task);
  score = calculateScore(numbers, operators);
  time = calculateTime(score);

  currentTask = { task: task, solution: parseInt(solution), score: score, time: time };

  const forClient = { ...currentTask };
  delete forClient.solution;
  return forClient;
};

const startCompetition = () => {
  clearTimeout(prepareTimer);
  clearTimeout(timer);
  validSolutions = [];

  io.to(rooms.COMPETITION).emit(wsMessages.COMPETITION_PREPARE);

  prepareTimer = setTimeout(() => {
    // Send the new task to all subscribed users.
    io.to(rooms.COMPETITION).emit(wsMessages.COMPETITION_NEW_TASK, getNewTask());

    // Set the flag.
    competitionInProgress = true;

    // Time limit for the task.
    const time = currentTask.time * 1000;
    console.log(`[TASK] New task sent. Time limit: ${currentTask.time}s. [SOLUTION]: ${currentTask.solution}`);

    // A timer that will send a prepare message, and after 2 sec start a new competition automatically.
    timer = setTimeout(() => {
      startCompetition();
    }, time);
  }, 2000);
};

// Sets the competition flag and clears the automatic timer.
const endCompetition = () => {
  console.log('[TASK] Task finished.');
  competitionInProgress = false;
  io.to(rooms.COMPETITION).emit(wsMessages.COMPETITION_PREPARE);
  clearTimeout(prepareTimer);
  clearTimeout(timer);
};

var mongoUrl = 'mongodb://mathUser:mathUserPassword@mongo:27017/matholympics';

// connect to MongoDB - At the first run it will fail, beacuse we need to create the db..
var connectWithRetry = function () {
  return mongoose.connect(mongoUrl, function (err) {
    if (err) {
      console.error('Failed to connect to mongo on startup - retrying in 2 sec', err);
      setTimeout(connectWithRetry, 2000);
    } else {
      init();
      app.use('/', require('./routes'));

      server.listen(3000, () => {
        console.log('Started WS on port 3200.');
      });
    }
  });
};
connectWithRetry();

const storeMessage = (msg) => {
  if (last12Message.length < 12) {
    last12Message.push(msg);
  } else {
    last12Message.shift();
    last12Message.push(msg);
  }
};
