# Mathematic Olympics

Kompetitív multiplayer játék.

## Indítás

Az alkalmazás dockerizálva van. A futtatáshoz a következő lépések szükségesek

```bash
cd compose
docker-compose up --build
```

## Felhasználás

Inicializálva van egy user: username: admin, password: admin
Ezen felül lehetőség van regisztrációra.

A bejelentkezett felhasználók tudnak csevegni egymással, megtekinthetik a toplistát, illetve be tudnak lépni a verseny szobába.

A versenyszobában minimum 2 játékos esetén automatikusan kapnak a versenyzők egy megoldandó feladatot. A helyes megoldások pontot érnek, beérkezési sorrendben súlyozva (Csak a top 3 leggyorsabb.)
